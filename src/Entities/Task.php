<?php

namespace Amirmsj\LaravelToDo\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'status',
    ];

    public function labels()
    {
        return $this->belongsToMany(Label::class, 'labels_tasks', 'task_id', 'label_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
