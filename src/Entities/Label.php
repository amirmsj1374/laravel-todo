<?php

namespace Amirmsj\LaravelToDo\Entities;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'label',
    ];

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'labels_tasks', 'label_id', 'task_id');
    }
}
