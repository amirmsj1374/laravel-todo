<?php

namespace Amirmsj\ToDoTest;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Amirmsj\LaravelToDo\Entities\Label;
use Amirmsj\LaravelToDo\Facades\LabelFacade;
use Amirmsj\LaravelToDo\Facades\ResponderFacade;
use Tests\TestCase;

class LabelControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $testUserData = [
        'name'     => 'testUser',
        'email'    => 'testUser@gmail.com',
        'password' => 'test password',
    ];

    protected $testLabelData = [
        'label' => 'test label' ,
    ];

    public function test_get_all_labels()
    {
        // mock
        LabelFacade::shouldReceive('getLabels');
        ResponderFacade::shouldReceive('getLabels')->with([])->andReturn(['labels' => []]);

        $user   = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->getJson('api/label/get/all');
        $result->assertStatus(200);
        $result->assertExactJson(['labels' => []]);
    }

    public function test_failed_require_validation_add_new_label()
    {
        $user   = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->postJson('api/label/add');
        $result->assertStatus(422);
        $result->assertJsonValidationErrors('label');
    }

    public function test_failed_unique_validation_add_new_label()
    {
        $label  = Label::create($this->testLabelData);
        $user   = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->postJson('api/label/add', ['label' => $label]);
        $result->assertStatus(422);
        $result->assertJsonValidationErrors('label');
    }

    public function test_happy_path_add_new_label()
    {
        $user   = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->postJson('api/label/add', $this->testLabelData);
        $result->assertStatus(200);
        $result->assertJson(['message' => 'New label is added.']);
    }
}
