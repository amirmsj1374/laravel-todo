<?php

namespace Amirmsj\ToDoTest;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Amirmsj\LaravelToDo\Entities\Label;
use Amirmsj\LaravelToDo\Entities\Task;
use Amirmsj\LaravelToDo\Facades\ResponderFacade;
use Amirmsj\LaravelToDo\Facades\TaskFacade;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $testUserData = [
        'name'     => 'testUser',
        'email'    => 'testUser@gmail.com',
        'password' => 'test password',
    ];

    protected $testTaskData = [
        'title'       => 'test title',
        'description' => 'test description',
        'status'      => 'Open',
    ];
    protected $testLabelData = [
        'label' => 'test label' ,
    ];

    public function test_failed_validation_add_new_task()
    {
        // mock
        TaskFacade::shouldReceive('addTask')->never();
        ResponderFacade::shouldReceive('addTask')->never();

        $user = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->post('api/task/add', []);
        $result->assertStatus(302);
    }

    public function test_add_new_task()
    {
        // mock
        TaskFacade::shouldReceive('addTask')->with($this->testTaskData);
        ResponderFacade::shouldReceive('addTask')->andReturn(['message' => 'New task is added.']);

        $user = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->postJson('api/task/add', $this->testTaskData);
        $result->assertStatus(200);
        $result->assertJson(['message' => 'New task is added.']);
    }

    public function test_no_task_found()
    {
        // mock
        ResponderFacade::shouldReceive('NoTaskFound')->andReturn(['error' => 'Nothing found.']);

        $user = User::make($this->testUserData);
        $result = $this->actingAs($user, 'api')->postJson('api/task/update/5000', $this->testTaskData);
        $result->assertJson(['error' => 'Nothing found.']);
    }

    public function test_update_task()
    {
        $user = User::make($this->testUserData);
        $task = Task::create($this->testTaskData);

        $edit_data = [
            'title'       => 'test title Edited',
            'description' => 'test description Edited',
            'status'      => 'Close',
        ];
        $result = $this->actingAs($user, 'api')->postJson('api/task/update/'.$task->id, $edit_data);
        $result->assertStatus(200);
        $result->assertJson(['message' => 'The task was edited.']);
    }

    public function test_change_status_of_task()
    {
        $user = User::make($this->testUserData);
        $task = Task::create($this->testTaskData);

        $edit_data = [ 'status' => 'Close' ];
        $result = $this->actingAs($user, 'api')->postJson('api/task/change/status/'.$task->id, $edit_data);
        $result->assertStatus(200);
        $result->assertJson(['message' => 'The task status was changed.']);
    }


    public function test_show_detail_of_task()
    {
        $user = User::make($this->testUserData);
        $task = Task::create($this->testTaskData);

        $result = $this->actingAs($user, 'api')->getJson('api/task/show/detail/'.$task->id);
        $result->assertStatus(200);
    }

    public function test_can_not_see_detail_of_task()
    {
        $user = User::create([
            'name'     => 'userForShowDetailTask',
            'email'    => 'userForShowDetailTask@gmail.com',
            'password' => 'test password',
        ]);
        $task = Task::create($this->testTaskData);

        $result = $this->actingAs($user, 'api')->getJson('api/task/show/detail/'.$task->id);
        $result->assertJson(['error' => 'You can not see details.']);;
    }

    public function test_get_all_task()
    {
        $user = User::make($this->testUserData);

        $result = $this->actingAs($user, 'api')->getJson('api/task/get/all');
        $result->assertStatus(200);
        $result->assertExactJson(['tasks' => []]);
    }

    public function test_add_label_to_task()
    {
        $user = User::make($this->testUserData);
        $task = Task::create($this->testTaskData);
        $label = Label::create($this->testLabelData);

        $result = $this->actingAs($user, 'api')->postJson('api/task/add/labels/to/'.$task->id, [$label->id]);
        $result->assertStatus(200);
        $result->assertJson(['message' => 'New labels is added to task.']);
    }
}
