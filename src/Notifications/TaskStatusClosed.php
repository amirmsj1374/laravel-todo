<?php

namespace Amirmsj\LaravelToDo\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskStatusClosed extends Notification implements ShouldQueue
{
    use Queueable;

    protected $taskTitle;

    /**
     * Create a new notification instance.
     *
     * @param task
     */
    public function __construct($taskTitle)
    {
        $this->taskTitle = $taskTitle;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The status of task "' .$this->taskTitle. '" is Close.')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'taskTitle' => $this->taskTitle
        ];
    }
}
