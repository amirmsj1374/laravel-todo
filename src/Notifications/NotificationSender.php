<?php

namespace Amirmsj\LaravelToDo\Notifications;

use Amirmsj\LaravelToDo\Notifications\TaskStatusClosed;
use Illuminate\Support\Facades\Notification;

class NotificationSender {
    public function send($user, $title)
    {
        Notification::send($user, new TaskStatusClosed($title));
    }
}
