<?php

namespace Amirmsj\LaravelToDo\Http\Controllers;

use Illuminate\Routing\Controller;
use Amirmsj\LaravelToDo\Facades\LabelFacade;
use Amirmsj\LaravelToDo\Facades\ResponderFacade;
use Amirmsj\LaravelToDo\Http\Requests\AddLabelRequest;

class LabelController extends Controller
{
    public function addLabel(AddLabelRequest $request)
    {
        LabelFacade::addLabel($request->label);

        return ResponderFacade::addLabel();;
    }

    public function getLabels()
    {
        $labels = LabelFacade::getLabels();

        return ResponderFacade::getLabels($labels);
    }
}
