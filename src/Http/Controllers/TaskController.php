<?php

namespace Amirmsj\LaravelToDo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Amirmsj\LaravelToDo\Facades\ResponderFacade;
use Amirmsj\LaravelToDo\Facades\TaskFacade;
use Amirmsj\LaravelToDo\Http\Requests\ChangeTaskStatusRequest;
use Amirmsj\LaravelToDo\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    public function addTask(TaskRequest $request)
    {
        TaskFacade::addTask($request->all());

        return ResponderFacade::addTask();
    }

    public function updateTask($id, TaskRequest $request)
    {

        $task = TaskFacade::getTaskById($id);

        if ($task == null) {
            return ResponderFacade::NoTaskFound();
        }

        TaskFacade::updateTask($task, $request->all());

        return ResponderFacade::updateTask();
    }

    public function changeStatus($id, ChangeTaskStatusRequest $request)
    {
        $task = TaskFacade::getTaskById($id);
        if ($task == null) {
            return ResponderFacade::NoTaskFound();
        }

        TaskFacade::changeStatus($task, $request->status);

        //email notification via observer out of main cycle
        //notification when status is close
        return ResponderFacade::changeStatus();

    }

    public function showDetail($id)
    {
        $task = TaskFacade::getTaskById($id);

        if ($task == null) {
            return ResponderFacade::NoTaskFound();
        }
        if ($task->user_id != auth()->id()) {
            return ResponderFacade::canNotSeeDetail();
        }

        return ResponderFacade::showTaskDetail($task);

    }

    public function getTasks()
    {
        $tasks = TaskFacade::getAllTasks();

        return ResponderFacade::getTasks($tasks);
    }

    public function addLabelsToTask($id, Request $request)
    {
        $task = TaskFacade::getTaskById($id);

        if ($task == null) {
            return ResponderFacade::NoTaskFound();
        }

        TaskFacade::addLabelsToTask($task, $request->labels);

        return ResponderFacade::addLabelsToTask();
    }
}
