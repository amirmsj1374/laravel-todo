<?php

namespace Amirmsj\LaravelToDo\Observers;

use Amirmsj\LaravelToDo\Entities\Task;
use Amirmsj\LaravelToDo\Facades\NotificationSenderFacade;

class TaskObserver
{
    public function updating(Task $task)
    {
      if($task->isDirty('status')){
        if ($task->status == 'Close') {
            NotificationSenderFacade::send(auth()->user(), $task->title);
        }
      }
    }
}
