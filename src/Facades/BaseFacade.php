<?php

namespace Amirmsj\LaravelToDo\Facades;

use Illuminate\Support\Facades\Facade;

abstract class BaseFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return static::class;
    }
    static function shouldProxyTo($class)
    {
        app()->singleton(self::getFacadeAccessor(), $class);
    }

}
