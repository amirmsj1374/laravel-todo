<?php

namespace Amirmsj\LaravelToDo\Facades;

use Amirmsj\LaravelToDo\Responses\VueResponses;

class ResponderFacade extends BaseFacade
{
    protected static function getFacadeAccessor()
    {
        return VueResponses::class;
    }
}
