<?php

namespace Amirmsj\LaravelToDo\Responses;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class VueResponses {

    public function addTask()
    {
        return response()->json([
            'message' => 'New task is added.'
        ], Response::HTTP_OK);
    }

    public function updateTask()
    {
        return response()->json([
            'message' => 'The task was edited.'
        ], Response::HTTP_OK);
    }

    public function changeStatus()
    {
        return response()->json([
            'message' => 'The task status was changed.'
        ], Response::HTTP_OK);
    }

    public function NoTaskFound()
    {
        return response()->json([
            'error' => 'Nothing found.'
        ], Response::HTTP_BAD_REQUEST);
    }

    public function canNotSeeDetail()
    {
        return response()->json([
                'error' => 'You can not see details.'
        ], Response::HTTP_BAD_REQUEST);
    }


    public function showTaskDetail($task)
    {
        return response()->json([
            'task'   => $task
        ], Response::HTTP_OK);
    }

    public function addLabelsToTask()
    {
        return response()->json([
            'message' => 'New labels is added to task.'
        ], Response::HTTP_OK);
    }

    public function addLabel()
    {
        return response()->json([
            'message' => 'New label is added.'
        ], Response::HTTP_OK);
    }

    public function getTasks($tasks)
    {
        return response()->json([
            'tasks' => $tasks
        ], Response::HTTP_OK);
    }

    public function getLabels($labels)
    {
        return response()->json([
            'labels' => $labels
        ], Response::HTTP_OK);
    }
}
