<?php

namespace Amirmsj\LaravelToDo\Providers;

use Illuminate\Support\ServiceProvider;
use Amirmsj\LaravelToDo\Entities\Task;
use Amirmsj\LaravelToDo\Facades\LabelFacade;
use Amirmsj\LaravelToDo\Facades\NotificationSenderFacade;
use Amirmsj\LaravelToDo\Facades\TaskFacade;
use Amirmsj\LaravelToDo\Notifications\NotificationSender;
use Amirmsj\LaravelToDo\Observers\TaskObserver;
use Amirmsj\LaravelToDo\Repositories\LabelRepository;
use Amirmsj\LaravelToDo\Repositories\TaskRepository;

class LaravelToDoServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');

        $this->publishes([
			realpath(__DIR__ .'/../Database/Migrations') => database_path('migrations')
		], 'migrations');

        $this->publishes([
			realpath(__DIR__ .'/../tests') => base_path('tests/Unit')
		], 'tests');

        //observer on Label
        Task::observe(TaskObserver::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        TaskFacade::shouldProxyTo(TaskRepository::class);
        LabelFacade::shouldProxyTo(LabelRepository::class);
        NotificationSenderFacade::shouldProxyTo(NotificationSender::class);

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

}
