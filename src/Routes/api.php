<?php

use App\User;
use Illuminate\Support\Facades\Route;
use Amirmsj\LaravelToDo\Facades\NotificationSenderFacade;
use Amirmsj\LaravelToDo\Http\Controllers\TaskController;
use Amirmsj\LaravelToDo\Http\Controllers\LabelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/task')->middleware(['auth:api', 'throttle:10,1'])->group(function() {
    Route::post('/add', [TaskController::class, 'addTask']);
    Route::post('/update/{id}', [TaskController::class, 'updateTask']);
    Route::post('/change/status/{id}', [TaskController::class, 'changeStatus']);
    Route::get('/show/detail/{id}', [TaskController::class, 'showDetail']);
    Route::post('/add/labels/to/{id}', [TaskController::class, 'addLabelsToTask']);
    Route::get('/get/all', [TaskController::class, 'getTasks']);

});

Route::prefix('/label')->middleware(['auth:api', 'throttle:10,1'])->group(function() {
    Route::post('/add', [LabelController::class, 'addLabel']);
    Route::get('/get/all', [LabelController::class, 'getLabels']);
});

// test route
if (app()->environment('local')) {
    Route::prefix('/test')->group(function() {
        Route::get('/send/email', function() {
            User::unguard();
            $user = User::make([
                'name'    => 'amirmsj',
                'email'     => 'amirmsj@gmail.com',
                'password' => 'amirmsj95'
            ]);
            NotificationSenderFacade::send($user, 'test task email');
        });
    });
}

