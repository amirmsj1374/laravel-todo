<?php

namespace Amirmsj\LaravelToDo\Repositories;

use Amirmsj\LaravelToDo\Entities\Task;

class TaskRepository {
    public function addTask($data)
    {
        Task::create([
            'title'       => $data['title'],
            'description' => $data['description'],
            'status'      => $data['status'],
            'user_id'     => auth()->id()
        ]);
    }

    public function updateTask($task, $data)
    {
        $task->update([
            'title'       => $data['title'],
            'description' => $data['description'],
            'status'      => $data['status']
        ]);
    }

    public function getTaskById($id)
    {
        return Task::find($id);
    }

    public function getAllTasks()
    {
        return auth()->user()->tasks()->with('labels')->get()->toArray();
    }

    public function changeStatus($task, $status)
    {
        $task->status = $status;
        $task->save();
    }

    public function addLabelsToTask($task, $labels)
    {
        $task->labels()->syncWithoutDetaching($labels);
    }
}
