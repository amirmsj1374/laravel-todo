<?php

namespace Amirmsj\LaravelToDo\Repositories;

use Amirmsj\LaravelToDo\Entities\Label;

class LabelRepository {
    public function addLabel($label)
    {
        Label::create([
            'label' => $label
        ]);
    }

    public function getLabels()
    {
        return Label::with('tasks')->whereHas('tasks',function ($query){
            $query->where('user_id', auth()->user()->id);
        })->get()->toArray();
    }
}
