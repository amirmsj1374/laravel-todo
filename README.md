# laravel-todo

`laravel-todo` is a laravel package which created for ToDo list and adding label them.

## Installation

1.To install through Composer, by run the following command:

```bash
composer require amirmsj/laravel-todo
```

2.For running tests of project You can run following command:

```bash
php artisan vendor:publish
```

3.Add provider to config/app.php file

```bash
    Amirmsj\LaravelToDo\Providers\LaravelToDoServiceProvider::class,
```

4.Create tabels via migrations in database

```bash
    php artisan migrate
```

5.Add relationship between users and tasks in user model

```bash
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
```
## Documentation

In package throttle middleware is used. so it is possible to encounter 429 error (to many request)
when tests are executed. 

set configs in env and and phpunit

Sending emails is done in queue out of main cycle of application. therefore running queue command is necessary.

For sending mail please use [mailtrap](https://mailtrap.io) config

## License

The MIT License (MIT).